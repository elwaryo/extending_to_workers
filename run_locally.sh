#!/usr/bin/env bash

docker build -t local/wirbel:0.0.1 .


docker stop airflow_web
docker stop airflow_worker
docker stop airflow_broker
docker stop airflow_db

docker network rm airflow_net

docker network create airflow_net

docker pull redis:5.0.9-alpine
docker run --net=airflow_net -d --rm --name airflow_broker -p 6379:6379 -e POSTGRES_PASSWORD=die_pass redis:5.0.9-alpine

docker pull postgres:12.3-alpine
docker run --net=airflow_net -d --rm --name airflow_db -p 5432:5432 -e POSTGRES_PASSWORD=die_pass postgres:12.3-alpine


DEBUG=''
if [ "$1" == 'DEBUG' ]; then
  DEBUG='--entrypoint sleep infinity'
fi

# Start the webserver
docker run --net=airflow_net -d --rm --name airflow_web -p 8080:8080 \
  -e AIRFLOW_DB_HOST=airflow_db \
  -e AIRFLOW_DB_ADMIN_USR=postgres \
  -e AIRFLOW_DB_ADMIN_USR_PASSWD=die_pass \
  -e AIRFLOW_DB_NAME=wirbel_db \
  -e AIRFLOW_DB_USR=wirbel_usr \
  -e AIRFLOW_DB_USR_PASSWD=das_pass \
  -e AIRFLOW_BROKER_URL=redis://airflow_broker/ \
  $DEBUG local/wirbel:0.0.1 webserver

# Start the worker
docker run --net=airflow_net -d --rm --name airflow_worker -p 8793:8793 \
  -e AIRFLOW_DB_HOST=airflow_db \
  -e AIRFLOW_DB_ADMIN_USR=postgres \
  -e AIRFLOW_DB_ADMIN_USR_PASSWD=die_pass \
  -e AIRFLOW_DB_NAME=wirbel_db \
  -e AIRFLOW_DB_USR=wirbel_usr \
  -e AIRFLOW_DB_USR_PASSWD=das_pass \
  -e AIRFLOW_BROKER_URL=redis://airflow_broker/ \
  $DEBUG local/wirbel:0.0.1 worker
